# vulnmath -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/vulnmath)

## Chal Info

Desc: `Enter the belly of the beast and emerge victorious.`

Given files:

* vulnmath
* libc.so.6

Hints:

* What all can printf do? Can you leverage it's capabilites?

Flag: `TUCTF{I_w45_w4rn3d_4b0u7_pr1n7f..._bu7_I_d1dn'7_l1573n}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/vulnmath)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/vulnmath:tuctf2019
```
